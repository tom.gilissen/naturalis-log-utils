package nl.naturalis.log;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/*
 *
 */
@JsonPropertyOrder({ "text", "channel", "username", "icon_url" })
public class Payload {
    private String text;
    private String channel;
    private String username;
    private String icon_url = "https://upload.wikimedia.org/wikipedia/commons/e/ef/OpenMoji-color_1F916.svg";

    public Payload(String text) {
        this.text = text;
    }

    public Payload(String text, String channel) {
        this(text);
        this.channel = channel;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        String payloadStr = null;
        try {
            payloadStr = objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return null;
        }
        // For compatibility with Slack incoming webhooks, if no Content-Type header
        // is set then the request body must be prefixed with payload=
        return String.format("payload=%s", payloadStr);
    }
}