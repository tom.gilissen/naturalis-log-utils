package nl.naturalis.log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/*
 * InstantMessenger is a simple class for sending instant messages
 * to either Mattermost and/or Slack channel(s).
 * Instantiate the class with an incoming webhook and you can use
 * it to send messages or payloads to a channel or a specified user.
 *
 */
public class InstantMessenger {

    private final String webhook;

    private String userName;
    private String iconUrl;

    private static final Logger logger = LogManager.getLogger(InstantMessenger.class);

    /**
     *
     *
     * @param webhook : the URL of the incoming webhook
     */
    public InstantMessenger(String webhook) {
        this.webhook = webhook;
    }

    /*
     * Set or change the user name of the sender of the message
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    /*
     * Set or change the icon url of the profile picture
     */
    public void setIconUrl() {
        this.iconUrl = iconUrl;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    /*
     * Send a text message to the default channel
     */
    public void sendMessage(String message) {
        Payload payload = new Payload(message);
        if (userName != null)
            payload.setUsername(userName);
        if (iconUrl != null)
            payload.setIcon_url(iconUrl);
        this.postPayload(payload);
    }

    /*
     * Send a text message to the specified channel
     *
     * To send a direct message to a specific person, make sure
     * to add a @ to the user name. E.g.:
     * client.sendMessageToChannel("Hi John", "@john.doe")
     */
    public void sendMessageToChannel(String message, String channel) {
        Payload payload = new Payload(message, channel);
        if (userName != null)
            payload.setUsername(userName);
        if (iconUrl != null)
            payload.setIcon_url(iconUrl);
        this.postPayload(new Payload(message, channel));
    }

    public void postPayload(Payload payload) {
        try {
            StringEntity entity = new StringEntity(payload.toString(), ContentType.APPLICATION_FORM_URLENCODED);
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost request = new HttpPost(webhook);
            request.setEntity(entity);
            HttpResponse response = httpClient.execute(request);
            if (response.getStatusLine().getStatusCode() == 200)
                logger.info("ok");
        } catch (IOException e) {
            logger.error("Failed to send message: " + e.getMessage());
        }
    }

}
